//
//  ViewController.swift
//  mundoanimal_22000356
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Kingfisher
import Alamofire

struct Dados: Codable{
    
    let image_link: String
    let name: String
    let latin_name: String
}

class ViewController: UIViewController {

    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var imagem: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of: Dados.self){ (response) in
            if let animal = response.value{
                self.imagem.kf.setImage(with: URL(string: animal.image_link))
                self.nome.text = animal.latin_name
                self.title = animal.name
            }
        }
    }


}

